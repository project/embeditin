Complete documentation is available at http://drupal.org/sandbox/pthurmond/1155976

*Getting started*
After you install the module, get an API key and Personal Identifier for your
website at http://embedit.in/api.
Enter the both on the configuration page (/admin/settings/embeditin).

After you begin publishing documents you will be able to see the files you
uploaded through the Drupal site under "My embeds" on embedit.in
(http://embedit.in/my).

*Creating Embedit.in Docs*
After you enter your API key and Personal Identifier, you should be able to go
ahead and create embeditin nodes. Attach a document and save the node (you
cannot save it unless you attach at least one file).

*Help*
Developers, You can find documentation for the API at http://embedit.in/api.
You may also contact me through my website at:
http://www.patrickthurmond.com/my-code-projects/embedit-in-integrator-a-drupal-6-module/

Project created by Patrick Thurmond - http://www.patrickthurmond.com
