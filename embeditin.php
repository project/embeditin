<?php
/*
 * @file
 * Unofficial Embedit.in PHP Class library
 *
 * Reference: http://embedit.in/api
 */
class Embeditin {
  public $apiKey;
  public $personalIdentifier;
  public $url;
  public $signature;
  public $error;
  public $errorMsg;
  public $docRaw;
  public $docResult;
  public $docIdentifier;
  public $createRequest;
  public $lastMethod;
  public $lastRequestParams;
  public $lastRequestURL;

  /**
   * Populate the basic properties of the object.
   *
   * @param string $api_key
   *   From http://embedit.in/api.
   * @param string $personal_identifier
   *   From http://embedit.in/api.
   * @param string $email
   *   The email address to find corresponding documents. This is the email you
   *   included when creating the embed. This is optional, and you'll get all of
   *   your embeds back if you leave it out.
   */
  function __construct($apiKey, $personalIdentifier, $email = '') {
    $this->apiKey             = $apiKey;
    $this->personalIdentifier = $personalIdentifier;
    $this->email              = $email;
    $this->signature          = $this->calculateSignature($apiKey, $email, $personalIdentifier);
    $this->url                = 'http://embedit.in/api/v1/';
    $this->createRequest      = array();
  }

  /**
   * Calculate the signature
   *
   * @param string $api_key
   *    From http://embedit.in/api.
   * @param string $email
   *    The email address to find corresponding documents. This is the email you
   *    included when creating the embed. This is optional, and you'll get all
   *    of your embeds back if you leave it out.
   * @param string $personal_identifier
   *    From http://embedit.in/api.
   *
   * @return
   *    The signature string.
   */
  function calculateSignature($apiKey, $personalIdentifier, $email = '') {
    return sha1($apiKey . $email . $personalIdentifier);
  }

  /**
   * Upload a document from a file
   *
   * @param string $file : full path to file (Reference: http://us.php.net/manual/en/function.curl-setopt.php - CURLOPT_POSTFIELDS notes)
   * @param string $url : URL to file to be uploaded
   * @param string $title : The title of the created embed. This is optional, and will default to the filename or url if you leave it out.
   * @param string $allowed_url : Restrict viewing to a specific web address. Be sure to include the 'http://'. This is optional, and viewing will be unrestricted if you leave it out.
   * @param bool $allow_print : Allow printing of the embedded document. This is optional, and will be true if you leave it out.
   * @param bool $allow_download : Allow downloading of the embedded document. This is optional, and will be true if you leave it out.
   *
   * @return The embedit.in document ID
   */
  function create($file = '', $url = '', $title = '', $allowed_url = '', $allow_print = FALSE, $allow_download = FALSE) {
    $method = 'create';

    $params = array(
      'title'          => $title,
      // If this is populated then the document will not be made public.
      'allowed_url'    => $allowed_url,
      'allow_print'    => (bool) $allow_print,
      'allow_download' => (bool) $allow_download,
    );

    $this->createRequest = $params;

    if (!empty($file)) {
      $params['file'] = "@$file"; //The @ sign tells PHP CURL to grab the file and send it as multipart/form-data encoded contents
    }

    if (empty($file) && !empty($url)) {
      $params['url'] = $url;
    }

    $result = $this->sendRequest($method, $params);
    $this->docIdentifier = $result;
    return $result;
  }

  /**
   * Get a list of the current users files
   *
   * @return array containing link, owner_email, allow_print, thumb, files, title, identifier, allow_download, public, and allowed_url for all documents
   */
  function getList() {
    $method = 'list/xml';
    $result = $this->sendRequest($method, array(), 'GET');
    return $result;
  }

  /**
   * Get a document's specific data from EmbedIt.in
   *
   * @return array containing link, owner_email, allow_print, thumb, files, title, identifier, allow_download, public, and allowed_url for all documents
   */
  function getSettings($identifier) {
    $list = $this->getList();

    //Loop through all the embedded files until we find a winner
    foreach ($list AS $document) {
      if ($document->identifier == $identifier) {
        return $document;
      }
    }

    return FALSE;
  }

  /**
   * Get the document identifier after a create
   *
   * @return The processed document result's identifier
   */
  function getDocIdentifier() {
    return $this->docIdentifier;
  }

  /**
   * Get the raw document result
   *
   * @return The raw, unprocessed document result.
   */
  function getDocRaw() {
    return $this->docRaw;
  }

  /**
   * Get the document result
   *
   * @return The processed document result.
   */
  function getDocResult() {
    return $this->docResult;
  }

  /**
   * Get the error number encountered
   *
   * @return The error number encountered.
   */
  function getError() {
    return $this->error;
  }

  /**
   * Get the error encountered
   *
   * @return The error encountered.
   */
  function getErrorMsg() {
    return $this->errorMsg;
  }

  /**
   * Delete a document
   *
   * @param string $document_identifier : The 10-character identifier for the document; for example, the document_identifier for http://embedit.in/IMAFoJ4xKU.swf would be IMAFoJ4xKU.
   *
   * @return Response results on success;
   */
  function delete($document_identifier) {
    $method = 'delete';
    $params['document_identifier'] = $document_identifier;

    $result = $this->sendRequest($method, $params);
    return $result;
  }

  /**
   * Adds in the commonly used parameters. Reduces excessive code.
   *
   * @param string $params : The current parameters array
   *
   * @return The expanded parameters array;
   */
  function addCommonParams($params = array()) {
    $params['api_key']    = $this->apiKey;
    $params['identifier'] = $this->personalIdentifier;
    $params['email']      = $this->email;
    $params['signature']  = $this->signature;

    return $params;
  }

  /**
   * Send a request to the EmbedIt.In server API
   *
   * @param string $method : The API function name. Example: list
   * @param string $params : The API function specific parameters
   *
   * @return Response results on success;
   */
  function sendRequest($method, $params = array(), $type = 'POST') {
    $params = $this->addCommonParams($params);
    $this->lastRequestParams = $params;
    $this->lastMethod        = $method;
    $url                     = $this->url . $method;
    $this->lastRequestURL    = $url;

    /*
     *  We have to separate out the communication and data response
     *  from the parsing of the data for two reasons. First because
     *  we have to be able to do two types of requests, each with
     *  different needs. And second, because some responses are in
     *  XML and others just in straight HTML.
     *
     *  This is why we could not just use a modified stream context
     *  with the SimpleXML functions built into PHP5. That and I
     *  decided that we needed to be able to parse in earlier
     *  versions of PHP. This will probably not continue with future
     *  versions of this library.
     */
    if ($type == 'POST') {
      $x = curl_init($url);
      curl_setopt($x, CURLOPT_HEADER, 0);
      curl_setopt($x, CURLOPT_POST, 1);
      curl_setopt($x, CURLOPT_POSTFIELDS, $params); //When sending files directly you must send an array instead of url encoding the post fields string.
      curl_setopt($x, CURLOPT_FOLLOWLOCATION, 0);
      curl_setopt($x, CURLOPT_RETURNTRANSFER, 1);
      $data = curl_exec($x);

      if ($data === false || $errorno = curl_errno($x)) {
        $this->error = $errorno;
        $this->errormsg = curl_error($x);
        curl_close($x);

        return FALSE;
      }

      curl_close($x);
    }
    else {
      $post_params = http_build_query($params);
      $data = @file_get_contents("$url?$post_params");
    }

    //Detect if the response is in XML
    if (stristr($data, "<?xml version='1.0' encoding='UTF-8'?>")) {
      if (function_exists('simplexml_load_string')) {
        $result = @simplexml_load_string($data);
      }
      else {
        $result = $this->embeditinParseXML($data);
      }
    }
    else { //If it is not XML then it is probably a link to the document
      $filters = array(
        '<html><body>You are being <a href="', //Remove the response HTML
        '">redirected</a>.</body></html>',
        'http://embedit.in/', //Remove the document domain
        '.swf' //Finally remove the file extension and you have the document ID
      );

      $result = str_ireplace($filters, '', $data);
    }

    $this->docRaw = $xml;
    $this->docResult = $result;
    return $result;
  }

  /**
   * Parse XML when SimpleXML is not present
   *
   * @param string $xml : The raw XML
   *
   * @return The object form of the XML;
   */
  function embeditinParseXML($xml) {
    if (!class_exists('EmbeditinObjectFromXML')) {
      include('objectfromxml.class.php');
    }

    $xml = new EmbeditinObjectFromXML();
    $xml->objectFromXML($xml);
  }
}
?>
