<?php

/*
 * @file
 * EmbedIt.in administration page
 */

/**
 * EmbedIt.in Configuration Form
 */
function embeditin_config_form() {
  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('EmbedIt.in API settings'),
    '#description' => t('This value is necessary for you to be able to make any API calls to EmbedIt.in. Set up an EmbedIt.in API account at <a href="http://www.embedit.in/">http://www.embedit.in</a>.'),
  );

  $form['api']['embeditin_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#required' => TRUE,
    '#default_value' => variable_get('embeditin_api_key', ''),
    '#description' => t('The embedit.in API key for your website.'),
  );

  $form['api']['embeditin_personal_identifier'] = array(
    '#type' => 'textfield',
    '#title' => t('Personal Identifier'),
    '#required' => TRUE,
    '#default_value' => variable_get('embeditin_personal_identifier', ''),
    '#description' => t('The embedit.in personal identifier for your account. This is required.'),
  );

  $form['nodes'] = array(
    '#type' => 'fieldset',
    '#title' => t('EmbedIt.in cusomizations'),
    '#description' => t('These settings affect the options available when documents are created.'),
  );

  $form['nodes']['embeditin_default_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Email Address'),
    '#default_value' => variable_get('embeditin_default_email', ''),
    '#description' => t('The default email address of the person using your application.'),
  );

  $form['nodes']['embeditin_choose_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show email option?'),
    '#default_value' => variable_get('embeditin_choose_email', 0),
    '#description' => t('Allow users to enter their email to attach to the document?'),
  );

  $form['nodes']['embeditin_default_private'] = array(
    '#type' => 'select',
    '#title' => t('Default visibility setting'),
    '#options' => embeditin_private_options(),
    '#default_value' => variable_get('embeditin_default_private', EMBEDITIN_DEFAULT_PRIVATE),
    '#description' => t('Private documents are only available to someone logged into the account that the document is attached to. This is not a recommended option to choose. Public documents are also available on embedit.in and are indexed by search engines.'),
  );

  $form['nodes']['embeditin_choose_private'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show visibility option?'),
    '#default_value' => variable_get('embeditin_choose_private', 1),
    '#description' => t('Allow users to choose whether their documents are private or public?'),
  );

  $form['nodes']['embeditin_default_print'] = array(
    '#type' => 'select',
    '#title' => t('Default print setting'),
    '#options' => embeditin_print_options(),
    '#default_value' => variable_get('embeditin_default_print', EMBEDITIN_DEFAULT_PRINT),
    '#description' => t('Allow visitors to print documents by default?'),
  );

  $form['nodes']['embeditin_choose_print'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show print option?'),
    '#default_value' => variable_get('embeditin_choose_print', 0),
    '#description' => t('EmbedIt.in offers the ability for documents to be downloaded.'),
  );

  $form['nodes']['embeditin_default_download'] = array(
    '#type' => 'select',
    '#title' => t('Default download setting'),
    '#options' => embeditin_download_options(),
    '#default_value' => variable_get('embeditin_default_download', EMBEDITIN_DEFAULT_DOWNLOAD),
    '#description' => t('Documents may be downloaded from EmbedIt.in'),
  );

  $form['nodes']['embeditin_choose_download'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show download option?'),
    '#default_value' => variable_get('embeditin_choose_download', 0),
    '#description' => t('Allow users to choose whether their documents can be downloaded?'),
  );

  $form['nodeview'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node view'),
    '#description' => t('Control the display of doc as full nodes.'),
  );

  $form['nodeview']['embeditin_show_doc_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show link to EmbedIt.in document.'),
    '#default_value' => variable_get('embeditin_show_doc_link', 0),
    '#description' => t('If checked, a link to view the document on embedit.in will be displayed.'),
  );

  return system_settings_form($form);
}
