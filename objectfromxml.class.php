<?php

/**
 * @file
 * PHP XML to Object Class library
 *
 * This is a class specifically designed to handle parsing XML
 * documents and turning them into usable objects.
 *
 * Truthfully I pieced this together from multiple comments and notes from PHP.net
 * so it isn't "entirely" mine. But this particular combination and build is.
 * Either way it is being distributed freely, so its all good.
 */

class EmbeditinObjectFromXML {
  public $parser;
  public $iter = 0;
  public $path = array();
  public $xml  = array();

  /**
   * Takes an XML string and turns it into an object.
   *
   * @return
   *   The XML object.
   */
  function objectFromXML($xml) {
    $this->parser = xml_parser_create();

    xml_set_object($this->parser, &$this);
    xml_parser_set_option($this->parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parser_set_option($this->parser, XML_OPTION_CASE_FOLDING, 0);
    xml_set_element_handler($this->parser, "handleTagStart", "handleTagEnd");
    xml_set_character_data_handler($this->parser, "handleTagCData");

    xml_parse($this->parser, $xml);
    xml_parser_free($this->parser);

    $this->xml = $this->xml['_children'][0];
  }

  /**
   * Gets eval path for the xml object to use.
   *
   * @return
   *   The eval path.
   */
  function getEvalPath() {
    return '$this->xml[' . "'" . implode("']['", $this->path) . "'" . ']';
  }

  /**
   * Callback for the opening of tags.
   */
  function handleTagStart($parser, $tag, $attributes) {
    array_push($this->path, '_children');
    array_push($this->path, ($this->iter++));

    $e = $this->getEvalPath();
    eval ($e . "['_name'] = \$tag;");

    if ($attributes !== array()) {
      eval ($e . "['_attributes'] = \$attributes;");
    }
  }

  /**
   * Callback for the cdata portions of the XML.
   */
  function handleTagCData($parser, $cdata) {
    $e = $this->getEvalPath();
    eval ($e . "['_value'] = \$cdata;");
  }

  /**
   * Callback for the closing of tags.
   */
  function handleTagEnd($parser, $tag) {
    array_pop($this->path);
    array_pop($this->path);
  }
}
